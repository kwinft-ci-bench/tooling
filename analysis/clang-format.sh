#!/usr/bin/env bash

set -eux
set -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SOURCE_DIR=$(dirname "$(dirname "$SCRIPT_DIR")")
RUN_SCRIPT_URL="$CI_SERVER_URL/kwinft-ci-bench/tooling/-/raw/master/analysis/run-clang-format.py"

echo "Running clang-format on the whole source."
python <(curl -s "$RUN_SCRIPT_URL") -r "$SOURCE_DIR"
