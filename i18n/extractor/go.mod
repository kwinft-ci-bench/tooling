module gitlab.com/kwinft/tooling/i18n/extractor

go 1.15

require (
	github.com/alecthomas/repr v0.0.0-20201120212035-bb82daffcca2
	github.com/bmatcuk/doublestar/v2 v2.0.4
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/urfave/cli/v2 v2.3.0
	gopkg.in/yaml.v2 v2.4.0
)
