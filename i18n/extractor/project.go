// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: MIT

package main

import (
	"os"
	"path"
)

// Project contains the deserialised form of the project
type Project struct {
	Outdir       string   `yaml:"outdir"`
	Translations []Subdir `yaml:"translations"`
}

// Subdir contains information about a project's subdirectory
type Subdir struct {
	Name      string   `yaml:"name"`
	Root      string   `yaml:"root"`
	Outdir    string   `yaml:"outdir"`
	Inputs    []string `yaml:"inputs"`
	ExtractRC []string `yaml:"extractrc"`
}

// PrepareOutdir prepares the out directory of the subdirectory and returns
// the path to it
func (s *Subdir) PrepareOutdir() (string, error) {
	if s.Outdir != "" {
		outdir := path.Join(reporoot, s.Root, s.Outdir)
		return outdir, os.MkdirAll(outdir, os.ModePerm)
	}
	return "", nil
}
