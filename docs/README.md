# Documentation Tooling
## Commit messages
Provides a CI job template
that uses [commitlint][commitlint]
with the [conventionalcommits preset][commitlint-preset]
and additional configuration in the project repo.
The configuration file is by default expected at the path:

    tooling/docs/commitlint.config.js

Installing the commitlint tool locally
and running it to check commits
is possible too.
You can orientate yourself at the [CI job][commitlint-script]
for how to do that.

## CI integration
The CI job template is provided in the `gitlab-ci` directory.
The default configuration path can be customized via a CI variable.

[commitlint]: https://github.com/conventional-changelog/commitlint
[commitlint-preset]: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-conventionalcommits
[commitlint-script]: https://gitlab.com/kwinft/tooling/-/blob/master/docs/gitlab-ci/commits.yml#L25-29
